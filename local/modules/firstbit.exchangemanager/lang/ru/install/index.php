<?php
$MESS["EXCHANGE_MANAGER_NAME"] = "Первый бит: Модуль обмена";
$MESS["EXCHANGE_MANAGER_DESCRIPTION"] = "Модуль обмена товарами";
$MESS["EXCHANGE_MANAGER_PARTNER_NAME"] = "Первый бит";
$MESS["EXCHANGE_MANAGER_PARTNER_URI"] = "https://1cbit.ru/";
$MESS["EXCHANGE_MANAGER_INSTALL_TITLE"] = "Установка модуля";
$MESS["EXCHANGE_MANAGER_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, 
необходимая модулю. Пожалуйста обновите систему.";

