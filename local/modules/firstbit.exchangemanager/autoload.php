<?php

use Bitrix\Main\Loader;

Loader::registerAutoLoadClasses(
    'firstbit.exchangemanager',
    array(
        'DataHelpers' => 'lib/Helpers/DataHelpers.php',
        'Events' => 'lib/Events/EventManager.php',
        )
);
