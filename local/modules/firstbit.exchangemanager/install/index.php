<?php

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);

class firstbit_exchangemanager extends CModule{

    public function __construct()
    {

        if(file_exists(__DIR__."/version.php")){

            $arModuleVersion = array();

            include_once(__DIR__."/version.php");

            $this->MODULE_ID            = str_replace("_", ".", get_class($this));
            $this->MODULE_VERSION       = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            $this->MODULE_NAME          = Loc::getMessage("EXCHANGE_MANAGER_NAME");
            $this->MODULE_DESCRIPTION  = Loc::getMessage("EXCHANGE_MANAGER_DESCRIPTION");
            $this->PARTNER_NAME     = Loc::getMessage("EXCHANGE_MANAGER_PARTNER_NAME");
            $this->PARTNER_URI      = Loc::getMessage("EXCHANGE_MANAGER_PARTNER_URI");
        }
        return false;
    }

    public function DoInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
    }

    public function DoUninstall()
    {
        ModuleManager::unregisterModule($this->MODULE_ID);

        $this->UnInstallDB();
   }

    public function unInstallFiles()
    {
        DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
    }

    public function UnInstallDB(){

        Option::delete($this->MODULE_ID);

        return false;
    }

}